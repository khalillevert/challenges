import numpy as np
import matplotlib.image as mpimg

 # codage des couleurs pour plus de facilité
rouge = [0.807, 0.066, 0.149]
blanc = [1, 1, 1]
bleu = [0, 0.129, 0.278]

hauteur = 210
largeur = 2*hauteur
# Image de départ, entièrement noire
image = np.zeros((hauteur, largeur,3))
# Remplissage
image[:, 0:largeur//3 ] = bleu
image[:, largeur//3:2*largeur//3 ] = blanc
image[:, 2*largeur//3:largeur ] = rouge
mpimg.imsave("image2.png", image)