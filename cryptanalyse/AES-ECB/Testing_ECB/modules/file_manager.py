import os
import subprocess


def prepare_(filename):
    if os.path.isfile('filename') == True:
        os.system('rm ' + filename)


def get_data_from_file(fileName):
    data2 = []
    stri = ''
    bytes_stri = ''
    if fileName == "fichier.cnf":
        prepare_(fileName)
        os.system("echo 'I am some data so encrypt me ' > fichier.cnf")
        data = open(fileName, "r")
        data2 = data.readlines()[0].encode('ascii')
        data.close()
    else:
        data = open(fileName, "r")
        liste_lines = data.readlines()
        for i in liste_lines:
            #data2.append(i.encode('ascii')) # in case if we want a list
            stri += i
            data.close()
        bytes_stri = stri.encode('ascii')
    return bytes_stri


def create_ciphred_file(ciphred_data, cyphred_file):
    prepare_(cyphred_file)
    ciphred_file = open(cyphred_file, "wb")
    ciphred_file.write(ciphred_data)


def read_encrypted_file(filename):
    if os.path.isfile(filename) == True:
        encrypted_data = open(filename, "rb")
        liste_encrypted_byte = []
        for x in encrypted_data:
            liste_encrypted_byte.append(x)
            return liste_encrypted_byte
    else:
        return 1
