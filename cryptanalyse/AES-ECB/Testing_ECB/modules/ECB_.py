from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
import base64
import os
import random


def key_generation(
        param):  # 0 => random key, 1, 'string ' the string  wille be the key
    key = b''
    if param == 0:
        return random.randbytes(16)
    elif param == 1:
        return b'a' * 16
    elif param == 2:
        return b'0' * 16
    else:
        if len(param) == 16:
            param = param.encode('ascii')
            return param
        else:
            return 1


def encryption(data, key):
    cipher_parameters = AES.new(key, AES.MODE_ECB)
    Ciphered = cipher_parameters.encrypt(pad(data, 16))
    return Ciphered


def decription(ciphred, key):
    cipher_parameters = AES.new(key, AES.MODE_ECB)
    Ciphered = unpad(cipher_parameters.decrypt(ciphred), 16)
    return Ciphered


def get_data_by_block(data, block):
    final_list = []
    temp_str = ''
    j = 0
    for i in range(1, len(data)):
        b = i
        if b % block == 0:
            final_list.append(data[j:b])
            j = j + block
    temp_str = (data[block * len(final_list):len(data)])
    final_list.append(temp_str)
    return final_list
