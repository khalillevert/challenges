
import optparse

import io



def dechiffre(chaine):
    liste=[]
    liste2=[]
    for decal in range(1,256):
        for let in chaine:  
            ch=chr((ord(let)+decal)%256)  
            liste.append(ch)
        chaine3=''.join(liste)
        liste=[]
        liste2.append(chaine3)
    return liste2







def main():
    parser= optparse.OptionParser("Utilisation pour decoder, -c<Secret a décoder>")
    parser.add_option('-c', dest='chaine', type="string", help='Veillez préciser une chaine corecte')
    (option, args) = parser.parse_args()
    fichier = open("ch7.bin","r",encoding='latin-1')
    chaine= fichier.readline()
    print (chaine)
    if chaine == None:
        parser.print_usage()
        exit(0)
    liste = dechiffre(chaine)
    for el in liste:
        print(el)
        







if __name__ == "__main__":
    main()