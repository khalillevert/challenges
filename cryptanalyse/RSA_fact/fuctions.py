def _format_hexa(hexa):
    return hexa.replace(":", "")


def str_to_Hexa(str_):
    tmp_list = ['', '']
    tmp_list[0] = int(str_, 16)
    tmp_list[1] = hex(tmp_list[0])
    return tmp_list


def modulus_to_hexa(modulus):
    tmp_modulus = _format_hexa(modulus)
    tmp_modulus = str_to_Hexa(tmp_modulus)
    return tmp_modulus
