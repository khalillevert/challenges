from Crypto.Cipher import AES 
from Crypto.Util.Padding import pad, unpad
import base64
import os

os.system("touch fichier")

message = b'[id=546815648;name=test;is_member=false;mail=aaaa;pad=000000000]'
key=b"zzunecledeseaize"
IV=b"azertyuikjhgfdsq"

encryption_suite = AES.new(key, AES.MODE_CBC, IV)
padded_message = pad(message,16)
encrypted_message = encryption_suite.encrypt(padded_message)
encoded_b64=base64.b64encode(encrypted_message)
fichier = open("fichier", "w")
fichier.write(encoded_b64.decode('utf-8')+"\n")

decryption_suite = AES.new(key, AES.MODE_CBC, IV)
decrypted_message= decryption_suite.decrypt(encrypted_message)
unpaded_decrypted_message= unpad(decrypted_message, 16)

print(encoded_b64)
print(unpaded_decrypted_message)

fichier.close()