/**
 * @author Khalil  MARREF
 * @email khalilmarref1991@gmail.com
 * @create date 20-09-2021 09:18:53
 * @modify date 20-09-2021 09:18:53
 * @desc [description]
 */

#include<stdlib.h>
#include<stdio.h>

#define table_size 10

int run_shell()
{
   int status = system("/bin/bash");
   return status;
}

int main()
{   
    system("/bin/bash");
    int buf[table_size] = {0}; 
    for (int i = 0 ; i<table_size; i++)
    {
        printf("valeur tableau %d = %d \n", i,buf[i]);
    }

    int status =  run_shell();
    printf("le status de la fonction %d \n", status);
    return 0;
}