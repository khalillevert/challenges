/**
 * @author Khalil  MARREF
 * @email khalilmarref1991@gmail.com
 * @create date 12-12-2021 23:07:49
 * @modify date 12-12-2021 23:07:49
 * @desc [description]
 TESTING AFTER FREE
 */
#include<stdlib.h>
#include<stdio.h>

int main()
{
    int *p1 = malloc(sizeof(int));
    int *p2;
    *p1 = 5;
    printf("le pointeur de P1 = %p et la valeur de P2 = %p \n ",p1,p2);
    printf("le pointeur de P1 = %d et la valeur de P2 = %d \n",*p1,*p2);
    free(p1);

    p2 = malloc(sizeof(int));
    *p2 = 25;

    printf("le pointeur de P1 = %p et la valeur de P2 = %p \n",p1,p2);
    printf("le pointeur de P1 = %d et la valeur de P2 = %d \n",*p1,*p2);
    return 0; 

}