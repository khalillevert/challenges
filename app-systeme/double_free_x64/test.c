#include <stdio.h>
#include <unistd.h>
#define BUFSIZE1 512
#define BUFSIZE2 ((BUFSIZE1/2) - 8)

int main(int argc, char **argv) {
int *buf1 = malloc(sizeof(int));
int *buf2 = malloc(sizeof(int));
printf(" le pointeur du buf1 est le %p\n", buf1);
printf(" le pointeur du buf2 est le %p\n", buf2);
free(buf1);
free(buf2);
int *buf3 = malloc(sizeof(int));
int *buf4 = malloc(sizeof(int));
printf(" le pointeur du buf3 est le %p\n", buf3);
printf(" le pointeur du buf4 est le %p\n", buf4);
}