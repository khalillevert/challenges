struct Humain {
        int hp;
        void (*fire)(struct Zombie*);
        void (*prayChuckToGiveAMiracle)(); // pointeur sur le quelle on peut mettre l'addresse 
        void (*suicide)(struct Humain*);
        int living;
};
void prayChuckToGiveAMiracle() // the function which can read the flag 