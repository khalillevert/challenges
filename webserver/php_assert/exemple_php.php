<br /> 
 
<?php
 
if (isset($_GET['page'])) {
$page = $_GET['page'];
} else {
$page = "home";
}
 
$file = "templates/" . $page . ".php";
 
// I heard '..' is dangerous!
assert("strpos('$file', '..') === false") or die("Detected hacking attempt!");
 
// TODO: Make this look nice
assert("file_exists('$file')") or die("That file doesn't exist!");
 
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
 
<title>My PHP Website</title>
 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container">
<div class="navbar-header">
 
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
 
<a class="navbar-brand" href="#">Project name</a>
</div>
<div id="navbar" class="collapse navbar-collapse">
<ul class="nav navbar-nav">
<li>class="active"><a href="?page=home">Home</a></li>
<li>class="active"><a href="?page=about">About</a></li>
<li>class="active"><a href="?page=contact">Contact</a></li>
<!--<li class="active"><a href="?page=flag">My secrets</a></li> -->
</ul>
</div>
</div>
</nav>
 
<div class="container" style="margin-top:50px;">
 
 
</div>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js" />
</body>
</html>